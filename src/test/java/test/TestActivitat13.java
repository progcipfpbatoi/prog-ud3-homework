package test;

import org.junit.Test;

import prog.ud3.homework.Activitat13;

/**
 *
 * @author batoi
 */
public class TestActivitat13 extends TestCommon {
    
    
    private void testIncorrectDataType(String entry) {
       
        testOutputContainsText(
                entry, 
                "Error! El tipus de dades introduït és incorrecte".replaceAll(" ", "").toLowerCase(), 
                "No estás controlando la entrada de tipos incorrectos. Revisa el mensaje de error para la entrada '"+ entry +"' ");
    }
    
    
    @Test
    public void testIncorrectDataInFirstValue() {

        testIncorrectDataType("hola");
    };
    
    @Test
    public void testIncorrectDataInSecondValue() {

        testIncorrectDataType("2 hola");
    };
    
    @Test
    public void testIncorrectCardNumber() {

        String numCarta = "-3";
        testOutputContainsText(
                numCarta, 
                "El número de la carta és incorrecte".replaceAll(" ", "").toLowerCase(), 
                "No estás controlando que los números de carta estén fuera de rango. Revisa el mensaje de error para el número de carta '"+ numCarta +"' ");
    }
    
    @Test
    public void testIncorrectCardSuit() {

        String numCarta = "4";
        String numPalo = "0";
        String entrada = numCarta + " " +  numPalo;
        testOutputContainsText(
                entrada, 
                "El pal de la carta és incorrecte".replaceAll(" ", "").toLowerCase(), 
                "No estás controlando que los números de carta estén fuera de rango. Revisa el mensaje de error para el palo '"+ numPalo +"' ");
    }
    
    private void testRightCardShowed(String entrada, String nombreCarta){
        testOutputContainsText(
                entrada, 
                nombreCarta.replaceAll(" ", "").toLowerCase(), 
                "No estás mostrando la carta "+ nombreCarta + " cuando toca");
    }
    
     @Test
    public void testRightCardEntered() {

        String numCarta = "1";
        String numPalo = "1";
        String entrada = numCarta + " " +  numPalo;
        String nombreCarta = "As d'ors";
        testRightCardShowed(entrada, nombreCarta);
    }

    @Test
    public void testRightCardEntered2() {

        String numCarta = "1";
        String numPalo = "2";
        String entrada = numCarta + " " +  numPalo;
        String nombreCarta = "As de copes";
        testRightCardShowed(entrada, nombreCarta);
    }

    @Test
    public void testRightCardEntered3() {

        String numCarta = "12";
        String numPalo = "3";
        String entrada = numCarta + " " +  numPalo;
        String nombreCarta = "Rei de bastos";
        testRightCardShowed(entrada, nombreCarta);
    }
    
    @Test
    public void testRightCardEntered4() {

        String numCarta = "11";
        String numPalo = "4";
        String entrada = numCarta + " " +  numPalo;
        String nombreCarta = "Cavall d'espases";
        testRightCardShowed(entrada, nombreCarta);
    }

    @Test
    public void testRightCardEntered5() {

        String numCarta = "11";
        String numPalo = "4";
        String entrada = numCarta + " " +  numPalo;
        String nombreCarta = "Cavall d'espases";
        testRightCardShowed(entrada, nombreCarta);
    }
    
    @Test
    public void testRightCardEntered6() {

        String numCarta = "10";
        String numPalo = "1";
        String entrada = numCarta + " " +  numPalo;
        String nombreCarta = "Sota d'ors";
        testRightCardShowed(entrada, nombreCarta);
    }
    
    @Test
     public void testRightCardEntered7() {

        String numCarta = "8";
        String numPalo = "2";
        String entrada = numCarta + " " +  numPalo;
        String nombreCarta = "Huit de copes";
        testRightCardShowed(entrada, nombreCarta);
    }
     
     @Test
     public void testRequestNumberCardTextExists() {
        
         String numCarta = "1";
        String numPalo = "1";
        String entrada = numCarta + " " +  numPalo;
        
        testOutputContainsText(
                entrada, 
                "Introdueix el número de la carta [1-12]: ".replaceAll(" ", "").toLowerCase(), 
                "No estás mostrando correctamente el texto de petición del número de carta. Comprueba que concuerda con el mostrado en el enunciado");
     }
     
     @Test
     public void testRequestCardSuitTextExists() {
        
        String numCarta = "1";
        String numPalo = "1";
        String entrada = numCarta + " " +  numPalo;
        
        testOutputContainsText(
                entrada, 
                "Introdueix el pal de la carta [1-4]: ".replaceAll(" ", "").toLowerCase(), 
                "No estás mostrando correctamente el texto de petición del palo de carta. Comprueba que concuerda con el mostrado en el enunciado");
     }
     
    @Override
    protected void runMain() {
        Activitat13.main(null);
    }

    
    
}
