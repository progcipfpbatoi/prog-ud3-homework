/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.After;
import static org.junit.Assert.assertTrue;
import org.junit.Before;


/**
 *
 * @author batoi
 */
public abstract class TestCommon {
    
    private PrintStream oldOut;
    private InputStream oldIn;
    protected ByteArrayOutputStream baos;
    
    @Before
    public void before() {
        // Redirigir salida
        baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        oldOut = System.out;
        System.out.flush();
        System.setOut(ps);
    }
    
    protected void redirectInAndWriteInBuffer(String toWriteInBuffer) {
        InputStream inputStream = new ByteArrayInputStream(toWriteInBuffer.getBytes());
        BufferedInputStream in = new BufferedInputStream(inputStream);
        oldIn = System.in;
        System.setIn(in);
    }
    
    protected void testOutputContainsText(String entry, String textMustContain, String errorMessage) {
        
        redirectInAndWriteInBuffer(entry);
        runMain();
        testOutputContainsText(textMustContain, errorMessage);
    }
    
    private void testOutputContainsText(String textMustContain, String errorMessage) {
        
        String text = textMustContain.replaceAll(" ", "").toLowerCase();
        assertTrue(errorMessage ,getOutputFormattedText().contains(text));
    }
    
    protected void testOutputNotContainsText(String entry, String textMustContain, String errorMessage) {
        
        redirectInAndWriteInBuffer(entry);
        runMain();
        testOutputNotContainsText(textMustContain, errorMessage);
    }
    
    private void testOutputNotContainsText(String textMustContain, String errorMessage) {
        
        String text = textMustContain.replaceAll(" ", "").toLowerCase();
        assertTrue(errorMessage , !getOutputFormattedText().contains(text));
    }

    protected void testOutputEndsWithText(String entry, String textMustContain, String errorMessage) {
        redirectInAndWriteInBuffer(entry);
        runMain();
        testOutputEndsWithText(textMustContain, errorMessage);
    }
    
    private void testOutputEndsWithText(String textMustEnd, String errorMessage) {
        String resultado = textMustEnd.replaceAll(" ", "").toLowerCase();
        assertTrue(errorMessage ,getOutputFormattedText().endsWith(resultado));
    }
    
    
    protected void testOutputContainsTextAndNotEndsWithText(String entry, String textMustContain, String errorMessage) {
        redirectInAndWriteInBuffer(entry);
        runMain();
        testOutputContainsTextAndNotEndsWithText(textMustContain, errorMessage);
    }
    
    private void testOutputContainsTextAndNotEndsWithText(String text, String errorMessage) {
        
        text = text.replaceAll(" ", "").toLowerCase();
        assertTrue(errorMessage ,getOutputFormattedText().contains(text) && !getOutputFormattedText().endsWith(text));
    }
    
    
    protected void testOutputHasFormat(String entry, String format, String errorMessageFormatNotFound) {
        redirectInAndWriteInBuffer(entry);
        runMain();
        
        Pattern pat = Pattern.compile(format);
        String salida = getOutputFormattedText();
        Matcher mat = pat.matcher(salida);
        assertTrue(errorMessageFormatNotFound, mat.find());
    }
    
    protected void testFormatAndTextInOutput(
            String entry, 
            String format, String errorMessageBadFormat, 
            String textMustContain, String errorMessageTextNotFound) {
        
        testOutputHasFormat(entry, format, errorMessageBadFormat);
        String textoPrevioSalida = textMustContain.replaceAll(" ", "").toLowerCase();
        testOutputContainsText(textoPrevioSalida, errorMessageTextNotFound);
    }
    
    protected String getOutputFormattedText() {
        return baos.toString().replaceAll("\n","").replaceAll(" ", "").toLowerCase();
    }
    
    protected abstract void runMain();
    
    
    @After
    public void after() {
        // Reestablecer salida
        System.setOut(oldOut);
        System.setIn(oldIn);
    }
}
