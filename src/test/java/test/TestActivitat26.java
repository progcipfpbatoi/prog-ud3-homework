/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package test;

import org.junit.Test;

import prog.ud3.homework.Activitat26;
import static org.junit.Assert.assertTrue;


/**
 *
 * @author batoi
 */
public class TestActivitat26 extends TestCommon {
    
    @Override
    protected void runMain() {
        Activitat26.main(null);
    }
    
    /** Test para comprobar el gasto de intentos **/
    
    @Test
    public void testNotRemainingAttemptsInDifficultMode() {

        
        String textoGastoIntentos = "Ho senc. No l'has encertat";
        String textoFin = "Fi";
        String nivel = "difícil";
        
        super.testOutputEndsWithText(
                nivel + " 3 3 3 3 3", 
                (textoGastoIntentos+textoFin).replaceAll(" ", ""), 
                "\nNivel " + nivel + "\n. No se está mostrando el texto '" + textoGastoIntentos 
                    + "' y/o finalizando el juego con el texto '" + textoFin + "'");
    }
    
    @Test
    public void testNotRemainingAttemptsInEasyMode() {

        String textoGastoIntentos = "Ho senc. No l'has encertat";
        String textoFin = "Fi";
        String nivel = "fàcil";
        
        super.testOutputEndsWithText(
                nivel + " 1 1 1 1 1 1 1 1 1 1", 
                (textoGastoIntentos+textoFin).replaceAll(" ", ""), 
                "\nNivel " + nivel +"\n. No se está mostrando el texto '" + textoGastoIntentos 
                    + "' y/o finalizando el juego con el texto '" + textoFin + "'");
    }

    @Test
    public void testNotRemainingAttemptsInMediumMode() {

        String textoGastoIntentos = "Ho senc. No l'has encertat";
        String textoFin = "Fi";
        String nivel = "intermedi";
        
        super.testOutputEndsWithText(
                nivel + " -10 -20 -30 -40 -30 20 10 5", 
                (textoGastoIntentos+textoFin).replaceAll(" ", ""), 
                "\nNivel " + nivel +"\n. No se está mostrando el texto '" + textoGastoIntentos 
                    + "' y/o finalizando el juego con el texto '" + textoFin + "'");
        
    }
    
    /** Fin Test para comprobar el gasto de intentos **/
    
    /** Test para comprobar el número acertado **/
    
    @Test
    public void testWinningGameIn1AttemptInDifficultMode() {

        String textoJuegoGanado = "Enhorabona! L'has encertat";
        String textoFin = "Fi";
        String nivel = "difícil";
        
        super.testOutputEndsWithText(
                nivel + " -45", 
                (textoJuegoGanado+textoFin).replaceAll(" ", ""), 
                "\nAcierto del número en el intento 1 en el nivel " + nivel + "\n. No se está mostrando el texto '" + textoJuegoGanado 
                    + "' y/o finalizando el juego con el texto '" + textoFin + "'");
    }

    @Test
    public void testWinningGameIn10AttemptsInEasyMode() {

        String textoJuegoGanado = "Enhorabona! L'has encertat";
        String textoFin = "Fi";
        String nivel = "fàcil";
        
        super.testOutputEndsWithText(
                nivel + " 1 1 1 1 1 1 1 1 1 -45", 
                (textoJuegoGanado+textoFin).replaceAll(" ", ""), 
                "\nAcierto en el intento 10 en el nivel "+nivel+"\n. No se está mostrando el texto '" 
                        + textoJuegoGanado + "' y finalizando el juego con el texto '" + textoFin + "'");
        
    }

    @Test
    public void testWinningGameIn5AttemptsInMediumMode() {

        String textoJuegoGanado = "Enhorabona! L'has encertat";
        String textoFin = "Fi";
        String nivel = "intermedi";
        
        super.testOutputEndsWithText(
                nivel + " 0 -10 -20 -30 -45", 
                (textoJuegoGanado+textoFin).replaceAll(" ", ""), 
                "\nAcierto en el intento 5 en el nivel "+nivel+"\n. No se está mostrando el texto '" 
                        + textoJuegoGanado + "' y finalizando el juego con el texto '" + textoFin + "'");
        
    }
    
    /** Fin Test para comprobar el número acertado **/
    
    
    /** Test introducción números a adivinar fuera de rango **/
    
    @Test
    public void testWrongNegativeNumberInAttempt1() {

        String textoNumeroIncorrecto = "Número incorrecte. Introduïx-lo de nou";
        String textoIntento1 = "Intent 1 [-99 - 99]:";
        String nivel = "fàcil";
        
        super.testOutputContainsTextAndNotEndsWithText(
                nivel + " -100 -45", 
                (textoNumeroIncorrecto+textoIntento1).replaceAll(" ", ""), 
                "\nNo se está mostrando el texto '" + textoNumeroIncorrecto + "' y volviendo a pedir número en primer intento con texto '" + textoIntento1 + "' cuando se introduce un número fuera de rango");
    }
    
    @Test
    public void testWrongPositiveNumberInAttempt1() {

        String textoNumeroIncorrecto = "Número incorrecte. Introduïx-lo de nou";
        String textoIntento = "Intent 1 [-99 - 99]:";
        String nivel = "fàcil";
        
        super.testOutputContainsTextAndNotEndsWithText(
                nivel + " 100 1 1 1 1 1 1 1 1 1 -45", 
                (textoNumeroIncorrecto+textoIntento).replaceAll(" ", ""), 
                "\nNo se está mostrando el texto '" + textoNumeroIncorrecto + "' y volviendo a pedir número en primer intento con texto '" + textoIntento + "' cuando se introduce un número fuera de rango");
    }

    @Test
    public void testWrongNegativeNumberInAttempt3() {

        String textoNumeroIncorrecto = "Número incorrecte. Introduïx-lo de nou";
        String textoIntento = "Intent 3 [-99 - 99]:";
        String nivel = "fàcil";
        
        super.testOutputContainsTextAndNotEndsWithText(
                nivel + " 0 -10 -1000 -20 -30 -45", 
                (textoNumeroIncorrecto+textoIntento).replaceAll(" ", ""), 
                "\nEntrada: 0 intentos -> Números: 0 -10 -1000 -20 -30 -45\n. No se está mostrando el texto '" 
                        + textoNumeroIncorrecto + "' y volviendo a pedir número en tercer intento con texto '" + textoIntento + "' cuando se introduce un número fuera de rango");
        
    }
    
    /** Fin Test introducción números a adivinar fuera de rango **/
    
    /** Test formato de la cabecera **/
    
    @Test
    public void testHeader() {
        
        String nivel = "fàcil";
        String textoCabecera =
                   "BENVINGUT AL JOC: ENDEVINA EL NÚMERO SECRET" 
                + "\n-------------------------------------------";
        
        testOutputContainsTextAndNotEndsWithText(
                nivel + " -45", 
                textoCabecera.replaceAll("\n", "").replaceAll(" ", "").toLowerCase(), 
                "\nNo se está comenzando el juego con el texto\n '" + textoCabecera + "'\n");
    }
    
    /** Fin test formato de la cabecera **/
    
    /** Test pistas mostradas correctas **/
    
    private void testClueInLevel(String level, String entry, String clueText, String colorFormat, String colorName) {
        
        String textoPistaConColor = colorFormat + clueText;
        
        testOutputContainsText(
                level + entry, 
                clueText.toLowerCase(), 
                "\nNo se está mostrando el texto '" + clueText + "' cuando procede");
        
        testOutputContainsText(
                level + entry, 
                textoPistaConColor.toLowerCase(), 
                "\nLa pista '" + clueText + "' no se está mostrando en color " + colorName);
    }
    @Test
    public void testClueCremant() {

        String nivel = "fàcil";
        String textoPista = "Cremant";
        String formatoColor = "[31m";
        
        testClueInLevel(nivel, " -44 -45", textoPista, formatoColor,  "rojo");
    }

     @Test
    public void testClueCalent() {

        String nivel = "fàcil";
        String textoPista = "Calent";
        String formatoColor = "[35m";
        
        testClueInLevel(nivel, " -34 -45", textoPista, formatoColor,  "púrpura/violeta");
    }
   
    @Test
    public void testClueTemplat() {

        String nivel = "fàcil";
        String textoPista = "Templat";
        String formatoColor = "[33m";
        
        testClueInLevel(nivel, " -25 -45", textoPista, formatoColor,  "amarillo");
    }

     @Test
    public void testClueFret() {

        String nivel = "fàcil";
        String textoPista = "Fret";
        String formatoColor = "[34m";
        
        testClueInLevel(nivel, " 50 -45", textoPista, formatoColor,  "azul");
    }

    @Test
    public void testDontShowClueOnLastAttempt() {

        String nivel = "fàcil";
        String entrada = " -45";
        
        String textoIntento1 = "Intent 1 [-99 - 99]:";
        String textoSiguiente = "Enhorabona! L'has encertat";
       
        String textoDebeAparecer = textoIntento1.replaceAll(" ", "")+textoSiguiente.replaceAll(" ", "");
        
        testOutputContainsText(
                nivel + entrada, 
                textoDebeAparecer.toLowerCase(), 
                "\nNo se está mostrando el texto '" + textoSiguiente + "' tras acertar el número secreto. "
                        + "\nObserva que no hay que mostrar la pista si se ha acertado el número");
    }
    
    @Test
    public void testDontShowTemplatAtALevelOtherThanEasy() {
        
        String nivel = "intermedi";
        String entrada = " -20 -45";
        String textoPista = "Templat";
        testOutputNotContainsText(
                nivel + entrada, 
                textoPista.toLowerCase(), 
                "\nEn el nivel " + nivel + " se está mostrando el texto '" + textoPista + "' cuando esta pista no debe mostrarse en este nivel. ");
    }
    
    @Test
    public void testDontShowCremantAtALevelDifficult() {
        
        String nivel = "difícil";
        String entrada = " -44 -45";
        String textoPista = "Cremant";
        testOutputNotContainsText(
                nivel + entrada, 
                textoPista.toLowerCase(), 
                "\nEn el nivel " + nivel + " se está mostrando el texto '" + textoPista + "' cuando esta pista no debe mostrarse en este nivel. ");
    }

    /** Fin test pìstas mostradas correctas **/
    
    /** Test tipos incorrectos son controlados */
    
    @Test
    public void testWrongInputTypeOnAttempt1() {
        
        String textoDebeAparecer ="Error! El tipus de dades introduït és incorrecte";
        testOutputContainsText(
                "fàcil hola", 
                textoDebeAparecer.toLowerCase(), 
                "Al introducir un tipo incorrecto (por ejemplo el texto hola cuando se pide un intento), "
                        + "debes mostrar el texto '" + textoDebeAparecer + "' y finalizar el programa");
    }

    @Test
    public void testWrongInputTypeOnAttempt1WithDecimalNumber() {
        
        String textoDebeAparecer ="Error! El tipus de dades introduït és incorrecte";
        testOutputContainsText(
                "fàcil 2.3", 
                textoDebeAparecer.toLowerCase(), 
                "Al introducir un tipo incorrecto (por ejemplo un número decimal cuando se pide un intento), "
                        + "debes mostrar el texto '" + textoDebeAparecer + "' y finalizar el programa");
    }
    
    /** Fin test tipos incorrectos son controlados **/ 
    
    /** Test nivel de dificultad incorrecto **/
    
    @Test
    public void testWrongOptionDifficultyLevel() {

        String textoIntentosIncorrectos = "Error! Nivell incorrecte";
        String textoPeticionIntentos = "Selecciona el nivell [fàcil|intermedi|difícil]:";
        
        testOutputContainsText(
                "hola fàcil -45", 
                textoIntentosIncorrectos + textoPeticionIntentos, 
                "\nAl introducir un nivel de juego inexistente, no se está mostrando el texto '" 
                        + textoIntentosIncorrectos 
                        + "' y volviendo a pedir el nivel del juego con el texto '" + textoPeticionIntentos + "'");
    }

    @Test
    public void testTwiceWrongAttempsGivesRightAnswer() {

        String textoIntentosIncorrectos = "Error! Nivell incorrecte";
        String textoPeticionIntentos = "Selecciona el nivell [fàcil|intermedi|difícil]:";
        
        testOutputContainsText(
                "FÀCIL INTERMEDI fàcil -45", 
                textoIntentosIncorrectos + textoPeticionIntentos, 
                "\nAl introducir un nivel de juego inexistente, no se está mostrando el texto '" 
                        + textoIntentosIncorrectos 
                        + "' y volviendo a pedir el nivel del juego con el texto '" + textoPeticionIntentos + "'");
    }
    
    /** Fin test nivel de dificultad incorrecto **/
}
