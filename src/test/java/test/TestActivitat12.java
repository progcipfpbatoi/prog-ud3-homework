package test;


import org.junit.Test;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import prog.ud3.homework.Activitat12;

/**
 *
 * @author batoi
 */
public class TestActivitat12 extends TestCommon {
    
    
    private void testIncorrectDataType(String entry) {
       
        testOutputContainsText(
                entry, 
                "Error! El tipus de dades introduït és incorrecte".replaceAll(" ", "").toLowerCase(), 
                "No estás controlando la entrada de tipos incorrectos. Revisa el mensaje de error para la entrada '"+ entry +"' ");
    }
     @Test
    public void testInvalidTypeEnteredFirstData() {

         testIncorrectDataType("aaa");
    }

    @Test
    public void testInvalidTypeEnteredSecondData() {

        testIncorrectDataType("200 hola");
    }
    
    private void testResultAndDescription(String peso, String altura, String resultado, String descripcion, boolean descompensado) {
        redirectInAndWriteInBuffer(peso + " " + altura);

        Activitat12.main(null);
        String salidaSinBlancosNiSaltosMinusculas = getOutputFormattedText();
        descripcion = descripcion.replaceAll(" ", "").toLowerCase();
        resultado = resultado.replaceAll(" ", "").toLowerCase();
        
        assertTrue("No estás cálculando correctamente el IMC para un peso de "+ peso + " y una altura de " + altura,salidaSinBlancosNiSaltosMinusculas.contains(resultado)); 
        assertTrue("Estás calculando correctamente el IMC pero no estás determinando correctamente su descripción para un peso de " + peso + " y una altura de " + altura,
                salidaSinBlancosNiSaltosMinusculas.contains(descripcion));
        
        if (descompensado) {
            assertTrue("No estás marcando el resultado como descompensado cuando sí lo es", salidaSinBlancosNiSaltosMinusculas.contains("descompensat"));
        }else {
            assertFalse("Estás marcando el resultado como descompensado cuando no lo es", salidaSinBlancosNiSaltosMinusculas.contains("descompensat"));
        }
    }
    
    @Test
    public void testLackOfWeightNotBalancedResult() {

        testResultAndDescription("74", "219", "13,554", "Falta de pes", true);    
    }
    
    @Test
    public void testLackOfWeightBalancedResult() {

        testResultAndDescription("50", "190", "13,063", "Falta de pes", false);   
    }
    
    @Test
    public void testNormalNotBalancedResult() {

        testResultAndDescription("74", "190", "19,333", "Normal", true);   
    }
    
    @Test
    public void testNormalBalancedResult() {

        testResultAndDescription("74", "179", "22,441", "Normal", false); 
    }
    
    @Test
    public void testOverweightBalancedResult() {

        testResultAndDescription("80", "170", "27,600", "Sobrepés", false);     
    }
    
    @Test
    public void testOverweight2BalancedResult() {

        testResultAndDescription("80", "160", "32,117", "Obés", false);     
    }
    
    @Test
    public void testObeseNotBalancedResult() {

        testResultAndDescription("70", "140", "39,239", "Obés", true);  
    }
    
    @Test
    public void testWeightTextExists() {

        testOutputContainsText(
                "70 140", 
                "Introdueix el teu pes (kg): ".replaceAll(" ", "").toLowerCase(), 
                "El texto para la solicitud del peso no es correcto. Revisa que esté bien escrito");
    }
    
    @Test
    public void testHeightTextExists() {

        testOutputContainsText(
                "70 140", 
                "Introdueix la teva alçada (cm): ".replaceAll(" ", "").toLowerCase(), 
                "El texto para la solicitud de la altura no es correcto. Revisa que esté bien escrito");
    }
    
    private void testNegativeData(String entry) {
       
        testOutputContainsText(
                entry, 
                "Error! Les dades introduïdes no són correctes".replaceAll(" ", "").toLowerCase(), 
                "No estás controlando la entrada de datos negativos. Revisa el mensaje de error para la entrada '"+ entry +"' ");
    }
    
    @Test
    public void testNegativeEntryInWeigth() {

        testNegativeData("-5");
    }

    @Test
    public void testNegativeEntryInHeigth() {

        testNegativeData("200 -6");
    }
    
    private void testOutputFormat(
            String entry, String format, String previousTextMustContain,
            String errorMessageBadFormat, String errorMessagePreviousText) {
        
        testOutputHasFormat(entry, format, errorMessageBadFormat);
        testOutputContainsText(entry, previousTextMustContain, errorMessagePreviousText);
    }
    
    @Test
    public void testFormatIMC() {

        testOutputFormat(
                "70 140", "[\\d]+[,][\\d]{3}", "El teu IMC (Oxford2003) és",
                "El formato de la salida del IMC no es correcto. Comprueba el número de decimales",
                "El texto previo al dato referente al IMC no es correcto. Comprueba que esté bien escrito");
    }
    
    @Override
    protected void runMain() {
        Activitat12.main(null);
    }
}
