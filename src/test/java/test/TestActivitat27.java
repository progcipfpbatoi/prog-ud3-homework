/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package test;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import prog.ud3.homework.Activitat27;
import static org.junit.Assert.assertTrue;


/**
 *
 * @author batoi
 */
public class TestActivitat27 extends TestCommon {
    
    @Test
    public void testHeader() {
        
        String textoCabecera ="--INTRODUCCIÓ DE MATRÍCULA--";
        
        testOutputContainsTextAndNotEndsWithText(
                "1 12", 
                textoCabecera.replaceAll("\n", "").replaceAll(" ", "").toLowerCase(), 
                "\nNo se está comenzando el programa con el texto\n '" + textoCabecera + "'\n");
    }
    
    @Test
    public void testCarLicensePlateNumberRequestedText() {

        testOutputContainsText(
                "0 B B B", 
                "\t-> Introdueix número:", 
                "El texto de solicitud del número de la matrícula no es correcto. "
                        + "Revisa que el texto coincide con el propuesto en el enunciado y que contiene un tabulador al inicio del mismo.");
    }

    
    @Test
    public void testCarLicensePlateFirstLetterRequestedText() {

        testOutputContainsText(
                "0 B B B", 
                "\t-> Introdueix primera letra:", 
                "El texto de solicitud de la primera letra de la matrícula no es correcto. "
                        + "Revisa que el texto coincide con el propuesto en el enunciado y que contiene un tabulador al inicio del mismo.");
    }
    
    @Test
    public void testCarLicensePlateSecondLetterRequestedText() {

        testOutputContainsText(
                "0 B B B", 
                "\t-> Introdueix segona letra:", 
                "El texto de solicitud de la segunda letra de la matrícula no es correcto. "
                        + "Revisa que el texto coincide con el propuesto en el enunciado y que contiene un tabulador al inicio del mismo.");
    }
    
    @Test
    public void testCarLicensePlateThirdLetterRequestedText() {

        testOutputContainsText(
                "0 B B B", 
                "\t-> Introdueix tercera letra:", 
                "El texto de solicitud de la tercera letra de la matrícula no es correcto. "
                        + "Revisa que el texto coincide con el propuesto en el enunciado y que contiene un tabulador al inicio del mismo.");
    }
    
    @Test
    public void testListOfCarLicensePlatesAreShown() {

        String ultimaMatriculaDebeAparecer = "1233 BBB(Flota: 1)";
        testOutputContainsText(
                "1234 B B B", 
                ultimaMatriculaDebeAparecer, 
                "La última matricula que debe aparece es la matrícula '" + ultimaMatriculaDebeAparecer + ". Si aún así la estás mostrando, puede que no la estés ubicando en la flota correcta ");
    }
    
    @Test
    public void testListOfCarLicensePlatesAreShown2() {

        String ultimaMatriculaDebeAparecer = "5554 BCB(Flota: 14)";
        testOutputContainsText("5555 B C B", 
                ultimaMatriculaDebeAparecer, 
                "La última matricula que debe aparece es la matrícula '" + ultimaMatriculaDebeAparecer + ". Si aún así la estás mostrando, puede que no la estés ubicando en la flota correcta ");
    }
    
    @Test
    public void testPreviousCarNumberTextIsCorrectlyShown() {

        String textoDebeAparecer = "Cotxes anteriors a 0000 BBB: 0";
        testOutputContainsText(
                "0 B B B", 
                textoDebeAparecer, 
                "El texto final que muestra el recuento de coches anteriores no es correcto o no aparece en su totalidad. Fíjate en el enunciado para mostrar exactamente la información requerida");
    }
    
    @Test
    public void testWrongInputTypeOnCarPlateNumber() {
        
        String textoDebeAparecer ="Error! El tipus de dades introduït és incorrecte";
        testOutputContainsText(
                "hola", 
                textoDebeAparecer.toLowerCase(), 
                "Al introducir un tipo incorrecto (por ejemplo el texto hola cuando se pide introducir el número de matrícula), "
                        + "debes mostrar el texto '" + textoDebeAparecer + "' y finalizar el programa");
    }
    
    @Test
    public void tesOutOfRangeNumberOnCarPlateNumber() {
        
        String textoDebeAparecer ="Matrícula no vàlida";
        testOutputContainsText(
                "99999 1234 B B B", 
                textoDebeAparecer.toLowerCase(), 
                "Al introducir un número fuera del rango permitido (por ejemplo el número 99999), "
                        + "debes mostrar el texto '" + textoDebeAparecer + "' y finalizar el programa");
    }
    
    
    @Test
    public void tesAmountOfCarsAreCorrectlyCalculated() {
        
        String textoDebeAparecer ="205555";
        testOutputContainsText(
                "5555 B C B", 
                textoDebeAparecer.toLowerCase(), 
                "No se están contando correctamente el número de matrículas anteriores a la matrícula introducida");
    }
    
    
    @Override
    protected void runMain() {
        
            Activitat27.main(null);
        
    }
    
}
