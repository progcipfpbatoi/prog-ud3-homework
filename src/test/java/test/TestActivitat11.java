package test;

import org.junit.Test;

import prog.ud3.homework.Activitat11;

public class TestActivitat11 extends TestCommon {

    private void testQuantityPaid(String entry, String result) {
        testOutputContainsText(
                entry, 
                result.replaceAll(" ", "").toLowerCase(), 
                "No estás calculando correctamente los impuestos a pagar para una persona que cobra " 
                        + entry + " euros mensuales");
    }

    @Test
    public void testNormalTaxes() {
        
        testQuantityPaid("4500,34", "16201,22");

    }

    @Test
    public void testHighTaxes() {
        
        testQuantityPaid("12050", "43380,00");

    }

    @Test
    public void testNoTaxes() {
        
        testQuantityPaid("500", "No has de pagar cap quantitat");

    }

    @Test
    public void testLowTaxes() {
        
        testQuantityPaid("900", "2160,00");

    }

    private void testQuantityPaidHelp(String entry, String result) {
        testOutputContainsText(
                entry, 
                result.replaceAll(" ", "").toLowerCase(), 
                "No estás indicando correctamente la información de la ayuda recibida (o no) por una persona que cobra " 
                        + entry + " euros mensuales. Comprueba que muestras la información igual que en el enunciado.");
    }

    @Test
    public void testNormalTaxesNoHelp() {
        
        testQuantityPaidHelp("4500,34", "no reps cap ajuda");

    }

    @Test
    public void testHighTaxesNoHelp() {
        
        testQuantityPaidHelp("12050", "no reps cap ajuda");

    }

    @Test
    public void testNoTaxesWithHelp() {
        
        testQuantityPaidHelp("500", "reps una ajuda de 1500€");

    }

    @Test
    public void testLowTaxesWithHelp() {
        
        testQuantityPaidHelp("900", "reps una ajuda de 1500€");

    }

    private void testAnualQuantityPaid(String entry, String result) {
         testOutputContainsText(
                entry, 
                result.replaceAll(" ", "").toLowerCase(), 
                "No estás calculando correctamente el sueldo anual para una persona que cobra "
                        + entry + " euros mensuales o bien no has acompañas la cantidad del símbolo del euro");
    }

    @Test
    public void testAnualNormalTaxes() {
        
        testAnualQuantityPaid("4500,34", "54004,08€");

    }

    @Test
    public void testAnualHighTaxes() {
        
        testAnualQuantityPaid("12050", "144600,00€");

    }

    @Test
    public void testAnualNoTaxes() {
        
        testAnualQuantityPaid("500", "6000,00€");

    }

    @Test
    public void testAnualLowTaxes() {
        
        testAnualQuantityPaid("900", "10800,00€");

    }
    
    @Test
    public void testRequestSalaryText() {

        String entry = "123";
        testOutputContainsText(
                entry, 
                "Introdueix el sou brut mensual(€):".replaceAll(" ", "").toLowerCase(), 
                "El texto de solicitud del sueldo no es correcto. Revisa que el texto coincide con el propuesto en el enunciado.");
    }
    
    @Test
    public void testFormatAnualSalaryText() {

        testFormatAndTextInOutput(
                "123", 
                "[\\d]+[,][\\d]{2}", "El formato de la salida del sueldo anual no es correcto. Comprueba que contiene el número de decimales indicado",
                "El teu sou anual ascendeix a un total de", "El texto previo al sueldo anual no es correcto. Comprueba que esté bien escrito");
    }

    @Test
    public void testFormatTaxesToPayText() {

        testFormatAndTextInOutput(
                "4000,23", 
                "[\\d]+[,][\\d]{2}€", "El formato de la salida de los impuestos a pagar no es correcto. Comprueba el número de decimales. ¿Has puesto el símbolo del euro tras la cantidad?",
                "Has de pagar", "El texto previo al dato referente a los impuestos a pagar no es correcto. Comprueba que esté bien escrito");
    }
    
    @Test
    public void testNegativeSalary() {
        
        testOutputContainsText(
                "-1", 
                "Error! Sou incorrecte".replaceAll(" ", "").toLowerCase(), 
                "No estás dando la salida correcta para los sueldos negativos");
    }

    @Test
    public void testInvalidEnterType() {
        
        String entry = "aaa";
        testOutputContainsText(
                entry, 
                "Error! El tipus de dades introduït és incorrecte".replaceAll(" ", "").toLowerCase(), 
                "No estás controlando la entrada de tipos incorrectos. Revisa el missatge de error per a l'entrada '"+entry+"'");
    }

    @Override
    protected void runMain() {
        Activitat11.main(null);
    }

    
}
