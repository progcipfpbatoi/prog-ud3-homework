
package prog.ud3.homework;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat26 {
      public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_RESET = "\u001B[0m";

    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        Random generadorAleatorios = new Random();

        int ns = (int) generadorAleatorios.nextInt(199) - 99;
        int intents = 1;
        int intentsmax = 0;
        int n;
        String dificultat = "";
        int diferencia;
        boolean dc = false;        

        System.out.println("BENVOLGUT AL JOC: ENDEVINA EL NÚMERO SECRET\n-------------------------------");
        while (dc == false) {
            System.out.println ("Selecciona el nivell [facil|intermedi|dificil]: ");
            if (teclat.hasNext()) {
                dificultat = teclat.next();
                switch (dificultat) {
                    case "facil":
                        dc = true;
                        intentsmax = 10;
                        break;
                    case "intermedi":
                        dc = true;
                        intentsmax = 8;
                        break;
                    case "dificil":
                        dc = true;
                        intentsmax = 5;
                        break;
                    default:
                        System.out.println("Error! Nivell Incorrecte");
                        break;
                }
            } else {
                System.out.println("Error! El tipus de dades introduït es incorrecte");
                return;

            }

        }
        do {
            System.out.println("Intent " + intents + "[-99 - 99]");
            if (teclat.hasNextInt()) {
                n = teclat.nextInt();
                if (n < -99 || n > 99) {
                    System.out.println("Numero incorrecte. Introduïx-lo de nou");
                    continue;
                }
            } else {
                System.out.println("Error! El tipus de dades introduït es incorrecte");
                return;
            }

            if (n == ns) {
                System.out.println("Enhorabona! L'has encertat");
                break;
            }

            diferencia = Math.abs(ns - n);

            switch (dificultat) {
                case "facil":
                    if (diferencia < 10) {
                        System.out.println(ANSI_RED + "Cremant" + ANSI_RESET);
                    } else if (diferencia >= 10 && diferencia < 20) {
                        System.out.println(ANSI_PURPLE + "Calent" + ANSI_RESET);
                    } else if (diferencia >= 20 && diferencia < 30) {
                        System.out.println(ANSI_YELLOW + "Templat" + ANSI_RESET);
                    } else if (diferencia > 30) {
                        System.out.println(ANSI_BLUE + "Fret" + ANSI_RESET);
                    }
                    break;

                case "intermedi":
                    if (diferencia < 10) {
                        System.out.println(ANSI_RED + "Cremant" + ANSI_RESET);
                    } else if (diferencia >= 10 && diferencia < 20) {
                        System.out.println(ANSI_PURPLE + "Calent" + ANSI_RESET);
                    } else if (diferencia >= 20) {
                        System.out.println(ANSI_BLUE + "Fret" + ANSI_RESET);
                    }
                    break;
                case "dificil":
                    if (diferencia < 20) {
                        System.out.println(ANSI_PURPLE + "Calent" + ANSI_RESET);
                    } else if (diferencia >= 20) {
                        System.out.println(ANSI_BLUE + "Fret" + ANSI_RESET);
                    }
                    break;
            }

            intents++;
            if (intents > intentsmax) {
                System.out.println("Ho senc, no l'has encertat");
                System.out.println("El teu número era: " + ns);
            }

        } while (intentsmax >= intents);

        System.out.println("Fi");
    }
}
