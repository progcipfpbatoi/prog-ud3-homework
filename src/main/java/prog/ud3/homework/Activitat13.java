package prog.ud3.homework;

import java.util.Scanner;


/**
 *
 * @author batoi
 */
public class Activitat13 {
 
 public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        int n;
        int pal;

        System.out.printf("Introdueix el número de la carta [1-12]: ");

        if (teclat.hasNextInt()) {
            n = teclat.nextInt();

            if (n < 1) {
                System.out.println("El número de la carta és incorrecte");
                return;
            }
            if (n > 12) {
                System.out.println("El número de la carta és incorrecte");
                return;
            }

        } else {
            System.out.println("Error! El tipus de dades introduït és incorrecte");
            return;
        }

        System.out.printf("Introdueix el pal de la carta [1-4]: ");

        if (teclat.hasNextInt()) {
            pal = teclat.nextInt();

            if (pal < 1) {
                System.out.println("El pal de la carta és incorrecte");
                return;
            }
            if (pal > 4) {
                System.out.println("El pal de la carta és incorrecte");
                return;
            }

        } else {
            System.out.println("Error! El tipus de dades introduït és incorrecte");
            return;
        }
        switch (n) {
            case 1:
                System.out.printf("As ");
                break;
            case 2:
                System.out.printf("Dos ");
                break;
            case 3:
                System.out.printf("Tres ");
                break;
            case 4:
                System.out.printf("Quatre ");
                break;
            case 5:
                System.out.printf("Cinc ");
                break;
            case 6:
                System.out.printf("Sis ");
                break;
            case 7:
                System.out.printf("Set ");
                break;
            case 8:
                System.out.printf("Huit ");
                break;
            case 9:
                System.out.printf("Nou ");
                break;
            case 10:
                System.out.printf("Sota ");
                break;
            case 11:
                System.out.printf("Cavall ");
                break;
            case 12:
                System.out.printf("Rei ");
                break;
        }
        switch (pal) {
            case 1:
                System.out.printf("d'ors/\n");
                break;
            case 2:
                System.out.printf("de copes\n");
                break;
            case 3:
                System.out.printf("de bastos\n");
                break;
            case 4:
                System.out.printf("d'espases\n");
                break;
        }

    }
}
