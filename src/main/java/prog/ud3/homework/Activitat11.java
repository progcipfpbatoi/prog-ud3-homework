
package prog.ud3.homework;

import java.util.Scanner;


/**
 *
 * @author batoi
 */
public class Activitat11 {
     
    public static void main(String[] args) {
        
        Scanner teclat = new Scanner(System.in);
        double sou;
        
        System.out.printf("Introdueix el sou brut mensual(€): ");
        
        if(teclat.hasNextFloat()) {
            sou = teclat.nextDouble();
            
         if(sou < 0){
             System.out.println("Error! Sou incorrecte");
             return;
         }
        }
        else {
            System.out.println("Error! El tipus de dades introduït és incorrecte");
            return;
        }
               
        double soua = sou * 12;        
        double pagar = soua * 0.2;        
        double pagarric = soua * 0.3;
        
         if (soua >= 15000 && soua <= 40000) {
            System.out.printf("El teu sou anual ascendeix a un total de: %.2f €\nHas de pagar: %.2f € i no reps cap ajuda", soua, pagar);
        } else if (soua > 40000) {
            System.out.printf("El teu sou anual ascendeix a un total de: %.2f €\nHas de pagar: %.2f € i no reps cap ajuda", soua, pagarric);
        } else if (soua >= 10000 && soua <15000) {
            System.out.printf("El teu sou anual ascendeix a un total de: %.2f €\nHas de pagar: %.2f € i reps una ajuda de 1500€", soua, pagar);
        } else if (soua >= 0 && soua < 10000) {
            System.out.printf("El teu sou anual ascendeix a un total de: %.2f €\nNo has de pagar cap quantitat i reps una ajuda de 1500€", soua); }
          else {                    
                    }
        }      
       
}
