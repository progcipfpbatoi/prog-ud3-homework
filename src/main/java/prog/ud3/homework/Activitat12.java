package prog.ud3.homework;

import java.util.Scanner;



/**
 *
 * @author batoi
 */
public class Activitat12 {
    
  
    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        int altcm;
        int pes;

        System.out.printf("Introdueix el teu pes (kg): ");

        if (teclat.hasNextInt()) {
            pes = teclat.nextInt();

            if (pes <= 0) {
                System.out.println("Error! Les dades introduïdes no són correctes");
                return;
            } 
        }else{
                System.out.println("Error! El tipus de dades introduït és incorrecte");
                return;
            }

        

        System.out.printf("Introdueix la teva alçada (cm): ");

        if (teclat.hasNextInt()) {
            altcm = teclat.nextInt();

            if (altcm <= 0) {
                System.out.println("Error! Les dades introduïdes no són correctes");
                return;
            }
        }else{
            System.out.println("Error! El tipus de dades introduït és incorrecte");
            return;
        }

        double alt = (double) altcm / 100;
        double IMC = (double) pes / Math.pow(alt, 2);
        double IMC2003 = (1.3 * pes) / Math.pow(alt, 2.5);
        double dif = Math.abs(IMC2003 - IMC);

        /* Casos: -Falta de pes (IMC2003 <15)
                      -Falta de pes descompensat (IMC2003 <15 i diferencia entre IMC > 1)
                      -Normal (IMC2003 >= 15 i IMC2003 < 25.5)
                      -Normal descompensat (IMC2003 >= 15 i IMC2003 < 25.5 i diferencia entre IMC > 1)
                      -Sobrepés (IMC2003 >= 25.5 i IMC2003 < 30.5)
                      -Sobrepés descompensat (IMC2003 >= 25.5 i IMC2003 < 30.5 i diferencia entre IMC > 1)
                      -Obés (IMC2003 > 30.5)
                      -Obés descompensat (IMC2003 > 30.5 i diferencia entre IMC > 1)*/
        if (IMC2003 < 15 && dif < 1) {
            System.out.printf("El teu IMC (Oxford2003) és %.3f (Falta de pes)", IMC2003);
        } else if (IMC2003 < 15 && dif > 1) {
            System.out.printf("El teu IMC (Oxford2003) és %.3f (Falta de pes descompensat)", IMC2003);
        } else if (IMC2003 >= 15 && IMC2003 < 25.5 && dif < 1) {
            System.out.printf("El teu IMC (Oxford2003) és %.3f (Normal)", IMC2003);
        } else if (IMC2003 >= 15 && IMC2003 < 25.5 && dif > 1) {
            System.out.printf("El teu IMC (Oxford2003) és %.3f (Normal descompensat)", IMC2003);
        } else if (IMC2003 >= 25.5 && IMC2003 < 30.5 && dif < 1) {
            System.out.printf("El teu IMC (Oxford2003) és %.3f (Sobrepés)", IMC2003);
        } else if (IMC2003 >= 25.5 && IMC2003 < 30.5 && dif > 1) {
            System.out.printf("El teu IMC (Oxford2003) és %.3f (Sobrepés descompensat)", IMC2003);
        } else if (IMC2003 >= 30.5 && dif < 1) {
            System.out.printf("El teu IMC (Oxford2003) és %.3f (Obés)", IMC2003);
        } else if (IMC2003 >= 30.5 && dif > 1) {
            System.out.printf("El teu IMC (Oxford2003) és %.3f (Obés descompensat)", IMC2003);

        }

    }
 
}
