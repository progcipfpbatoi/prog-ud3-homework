
package prog.ud3.homework;

import java.util.Scanner;

/**
 *
 * @author batoi
 */
public class Activitat27 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String matricula = "";
        int numeroMatricula = 0;
        char letraUno = 0;
        char letraDos = 0;
        char letraTres = 0;
        int contador = 0;

        while (true) {
            System.out.println("-- INTRODUCCIÓ DE MATRÍCULA --");
            int numero = 0;

            while (true) {

                System.out.print("   -> Introdueix número: ");
                if (scanner.hasNextInt()) {
                    numero = scanner.nextInt();
                    if (numero >= 0 && numero <= 9999) {
                        
                        numeroMatricula=numero;
                        break;
                    } else {
                        System.out.println("Matrícula no vàlida");
                        continue;

                    }
                } else {
                    System.out.println("Error! El tipus de dades introduït és incorrecte");
                    return;
                }

            }

            while (true) {

                System.out.print("   -> Introdueix primera letra: ");
                String letra1 = scanner.next();
                char letra1Char = letra1.charAt(0);

                if (letra1Char >= 'B' && letra1Char <= 'Z' && letra1Char != 'I' && letra1Char != 'E'
                        && letra1Char != 'O' && letra1Char != 'U' && letra1Char != 'Q') {

                    letraUno = letra1Char;

                } else {
                    System.out.println("Matrícula no vàlida");
                    break;
                }


                System.out.print("   -> Introdueix segona letra: ");
                String letra2 = scanner.next();
                char letra2Char = letra2.charAt(0);
                if (letra2Char >= 'B' && letra2Char <= 'Z' && letra2Char != 'I' && letra2Char != 'E'
                        && letra2Char != 'O' && letra2Char != 'U' && letra2Char != 'Q') {

                    letraDos = letra2Char;

                } else {
                    System.out.println("Matrícula no vàlida");
                    break;
                }



                System.out.print("   -> Introdueix tercera letra: ");
                String letra3 = scanner.next();
                char letra3Char = letra3.charAt(0);
                if (letra3Char >= 'B' && letra3Char <= 'Z' && letra3Char != 'I' && letra3Char != 'E'
                        && letra3Char != 'O' && letra3Char != 'U' && letra3Char != 'Q') {

                    letraTres = letra3Char;

                } else {
                    System.out.println("Matrícula no vàlida");
                    break;
                }
                break;
            }


        for (char j = 'B'; j <= 'Z'; j++) {
            if (j != 'I' && j != 'E' && j != 'O' && j != 'U' && j != 'Q') {
                for (char l = 'B'; l <= 'Z'; l++) {
                    if (l != 'I' && l != 'E' && l != 'O' && l != 'U' && l != 'Q') {
                        for (char m = 'B'; m <= 'Z'; m++) {  // Corregir 'l' a 'm'
                            if (m != 'I' && m != 'E' && m != 'O' && m != 'U' && m != 'Q') {
                                for (int i = 0; i <= 9999; i++) {
                                    System.out.printf("%04d %c%c%c(flota:%d)", i, j, l, m,obtenerFlota(contador) );
                                    System.out.printf("\r");
                                    if (i == numeroMatricula && j == letraUno && l == letraDos && m == letraTres) {
                                        System.out.printf("%04d %c%c%c(flota:%d)", i, letraUno, letraDos, letraTres,obtenerFlota(i));
                                        System.out.printf("\nCotxes anteriors a %04d %c%c%c: %d",i, letraUno, letraDos, letraTres, contador);

                                        return;
                                    } else {
                                        contador++;
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                    } else {
                        continue;
                    }
                }
            } else {
                continue;
            }
        }
        }


    }
        public static int obtenerFlota(int numero) {
        return (numero / 15600) + 1;
    }
}
